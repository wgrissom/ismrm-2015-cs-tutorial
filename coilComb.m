function y = coilComb(x,sens)

% 2015 Will Grissom, Vanderbilt University
% Adapted from Miki Lustig's SPIRiT code

y = 0;
for c = 1:size(sens,3)
    y = y + x(:,:,c).*conj(sens(:,:,c))./sum(abs(sens).^2,3);
end