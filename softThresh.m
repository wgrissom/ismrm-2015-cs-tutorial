function x = softThresh(y,t)

% 2015 Will Grissom, Vanderbilt University
% Adapted from Miki Lustig's SPIRiT code

% apply joint sparsity soft-thresholding
absy = abs(y);
unity = y./(repmat(absy,[1,1,size(y,3)])+eps);

res = max(absy-t,0);
x = unity.*res;