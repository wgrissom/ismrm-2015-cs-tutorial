load brain_8ch

img = ifft2c(DATA); % true image

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Reconstruction Parameters %%%%%%%%%%%%%%%%%%% 			
nIter = 200; % number of iteration
mask_type = 'pois_random'; % options are: 'unif','pois_random'
wavWeight = 0.00075;  % Wavelet soft-thresholding regularization in the reconstruction (SPIRiT only)

% load the sampling mask
switch mask_type
    case 'unif'
        % uniform 3x2 undersampling
        mask = mask_unif_nocalib.';
        
    case 'pois_random'
        % 2.5x by 2.5x Poisson disc with 25 by 25 cal region
        mask = mask_pois_2pt5x;
        %mask = vdPoisMex(200,200,20,20,2.5,2.5,25,0,0);
        
end

[nx,ny,nc] = size(DATA); % get data sizes
DATA = DATA.*repmat(mask,[1,1,nc]); % multiply with sampling mask

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% scale the data such that the zero-filled density compensated      %%%%%%%%%
% k-space norm is 1. This is useful in order to use similar         %%%%%%%%%
% regularization penalty values for different problems.             %%%%%%%%%

[~, dcomp] = getCalibSize(mask);  % get size of calibration area from mask
DATAcomp = DATA.*repmat(dcomp,[1,1,nc]);
scale_fctr = norm(DATAcomp(:))/sqrt(nc)/20;
DATA = DATA/scale_fctr;
DATAcomp = DATAcomp/scale_fctr;

img_dc = ifft2c(DATAcomp); % density-compensated zero-filled image
img = img/scale_fctr; % also scale fully-sampled image by this amount

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%		 Calculate sensitivity maps                        %%%%%%%%%%
disp('calculating sensitivity maps')
imgssq = sqrt(sum(abs(img).^2,3));
sens = img./repmat(imgssq,[1 1 nc]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%                  Reconstruction                        %%%%%%%%%

disp('calculating coil-combined density-compensated zero-filled image')
imgzf = coilComb(img_dc,sens);
imgzf_err = imgzf - imgssq;

disp('performing POCS SENSE reconstruction')
x = imgzf;
for n = 1:nIter
    
    % data consistency projection
    for c = 1:nc
        xc(:,:,c) = ifft2c(fft2c(sens(:,:,c).*x).*(1-mask)+mask.*DATA(:,:,c));
    end
    
    % coil combination
    x = coilComb(xc,sens);
    
    % show progress
    figure(1), imshow(abs(x),[0 0.5],'InitialMagnification',400); 
    h = text(140,190,num2str(n));
    set(h,'fontsize',64);
    set(h,'color',[0 1 0]);
    drawnow
    
end
imgsense = x;
imgsense_err = imgsense - imgssq;

disp('performing POCS l1-SENSE reconstruction')

% find the closest diadic size for the images, get wavelet xform operator
ssx = 2^ceil(log2(nx));
ssy = 2^ceil(log2(ny));
ss = max(ssx, ssy);
W = Wavelet('Daubechies',4,4);

% initial guess is density-compensated zero-filled
x = imgzf;
for n = 1:nIter
    
    % wavelet soft thresholding
    X = zpad(x,ss,ss); % zeropad to the closest diadic
    X = W'*softThresh(W*X,wavWeight); % threshold (magnitude)
    x = crop(X,nx,ny); % return to the original size
    
    % data consistency projection
    for c = 1:nc
        xc(:,:,c) = ifft2c(fft2c(sens(:,:,c).*x).*(1-mask)+mask.*DATA(:,:,c));
    end
    
    % coil combination
    x = coilComb(xc,sens);
    
    % show progress
    figure(1), imshow(abs(x),[0 0.5],'InitialMagnification',400); 
    h = text(140,190,num2str(n));
    set(h,'fontsize',64);
    set(h,'color',[0 1 0]);
    drawnow
    
end
imgl1sense = x;
imgl1sense_err = imgl1sense - imgssq;

figure
imshow(abs([0*imgssq imgzf_err imgsense_err imgl1sense_err]),[0 0.05],'InitialMagnification',400)
figure
imshow(abs([imgssq imgzf imgsense imgl1sense]),[0 0.5],'InitialMagnification',400)

% nrmse(imgssq,imgzf,1)
% nrmse(imgssq,imgsense,1)
% nrmse(imgssq,imgl1sense,1)



